#########################
## Application Context ##
#########################

## Set application context "Production"
page {
    headerData {
        3330001 = TEXT
        3330001.value (

<script type="text/javascript">
    var kitt3n_applicationContext = "Production";
</script>

        )
    }
}

## Override application context for "Production/Staging"
[applicationContext == "Production/Staging"]
    page {
        headerData {
            3330001.value (

<script type="text/javascript">
    var kitt3n_applicationContext = "Production/Staging";
</script>

            )
        }
    }
[END]

## Override application context for "Development"
[applicationContext == "Development*"]
    page {
        headerData {
            3330001.value (

<script type="text/javascript">
    var kitt3n_applicationContext = "Development";
</script>

            )
        }
    }
[END]


#########################
## WindowLoad Variable ##
#########################

## Set window.load variable
page {
    footerData {
        9999999 = TEXT
        9999999.value (

<script type="text/javascript">
    var kitt3n_windowLoad = true;
</script>

        )
    }
}


##################
## DNS Prefetch ##
##################

## Open head tag
[({$plugin.tx_kitt3nassets.settings.head.prefetch.google.fonts} == '1') || ({$plugin.tx_kitt3nassets.settings.head.prefetch.google.ajax} == '1') || ({$plugin.tx_kitt3nassets.settings.head.prefetch.cloudflare.cdnjs} == '1')]
page {
    headTag {
        cObject = COA
        cObject {
            10 = TEXT
            10.value = <head>
        }
    }
}
[END]

## Google Fonts
[{$plugin.tx_kitt3nassets.settings.head.prefetch.google.fonts} == '1']
    page.headTag.cObject.15 = TEXT
    page.headTag.cObject.15.value (
<link rel="dns-prefetch" href="//fonts.googleapis.com">
<link rel="preconnect" href="//fonts.googleapis.com">
    )
[END]

## Google AJAX
[{$plugin.tx_kitt3nassets.settings.head.prefetch.google.ajax} == '1']
    page.headTag.cObject.20 = TEXT
    page.headTag.cObject.20.value (
<link rel="dns-prefetch" href="//ajax.googleapis.com">
<link rel="preconnect" href="//ajax.googleapis.com">
    )
[END]

## Cloudflare CDN-JS
[{$plugin.tx_kitt3nassets.settings.head.prefetch.cloudflare.cdnjs} == '1']
    page.headTag.cObject.25 = TEXT
    page.headTag.cObject.25.value (
<link rel="dns-prefetch" href="//cdnjs.cloudflare.com">
<link rel="preconnect" href="//cdnjs.cloudflare.com">
    )
[END]


################
### Favicons ###
################

## favicon.ico
[fileExists('favicon.ico')]
    page.headerData.49 = TEXT
    page.headerData.49.data = getIndpEnv:TYPO3_SITE_URL
    page.headerData.49.dataWrap = <!--[if IE]><link rel="shortcut icon" href="|favicon.ico"><![endif]-->
[END]

## favicon.png
[fileExists('favicon.png')]
    page.headerData.50 = TEXT
    page.headerData.50.data = getIndpEnv:TYPO3_SITE_URL
    page.headerData.50.noTrimWrap (
|
<link rel="icon" type="image/png" href="|favicon.png">
|
    )
[END]

## favicon-16x16.png
[fileExists('favicon-16x16.png')]
    page.headerData.51 = TEXT
    page.headerData.51.data = getIndpEnv:TYPO3_SITE_URL
    page.headerData.51.noTrimWrap (
|
<link rel="icon" type="image/png" sizes="16x16" href="|favicon-16x16.png">
|
    )
[END]

## favicon-32x32.png
[fileExists('favicon-32x32.png')]
    page.headerData.52 = TEXT
    page.headerData.52.data = getIndpEnv:TYPO3_SITE_URL
    page.headerData.52.noTrimWrap (
|
<link rel="icon" type="image/png" sizes="32x32" href="|favicon-32x32.png">
|
    )
[END]

## favicon-96x96.png
[fileExists('favicon-96x96.png')]
    page.headerData.53 = TEXT
    page.headerData.53.data = getIndpEnv:TYPO3_SITE_URL
    page.headerData.53.noTrimWrap (
|
<link rel="icon" type="image/png" sizes="96x96" href="|favicon-96x96.png">
|
    )
[END]


#####################
### Android icons ###
#####################

## android-icon-36x36.png
[fileExists('android-icon-36x36.png')]
    page.headerData.54 = TEXT
    page.headerData.54.data = getIndpEnv:TYPO3_SITE_URL
    page.headerData.54.noTrimWrap (
|
<link rel="icon" type="image/png" sizes="36x36" href="|android-icon-36x36.png">
|
    )
[END]

## android-icon-48x48.png
[fileExists('android-icon-48x48.png')]
    page.headerData.55 = TEXT
    page.headerData.55.data = getIndpEnv:TYPO3_SITE_URL
    page.headerData.55.noTrimWrap (
|
<link rel="icon" type="image/png" sizes="48x48" href="|android-icon-48x48.png">
|
    )
[END]

## android-icon-72x72.png
[fileExists('android-icon-72x72.png')]
    page.headerData.56 = TEXT
    page.headerData.56.data = getIndpEnv:TYPO3_SITE_URL
    page.headerData.56.noTrimWrap (
|
<link rel="icon" type="image/png" sizes="72x72" href="|android-icon-72x72.png">
|
    )
[END]

## android-icon-96x96.png
[fileExists('android-icon-96x96.png')]
    page.headerData.57 = TEXT
    page.headerData.57.data = getIndpEnv:TYPO3_SITE_URL
    page.headerData.57.noTrimWrap (
|
<link rel="icon" type="image/png" sizes="96x96" href="|android-icon-96x96.png">
|
    )
[END]

## android-icon-144x144.png
[fileExists('android-icon-144x144.png')]
    page.headerData.58 = TEXT
    page.headerData.58.data = getIndpEnv:TYPO3_SITE_URL
    page.headerData.58.noTrimWrap (
|
<link rel="icon" type="image/png" sizes="144x144" href="|android-icon-144x144.png">
|
    )
[END]

## android-icon-192x192.png
[fileExists('android-icon-192x192.png')]
    page.headerData.59 = TEXT
    page.headerData.59.data = getIndpEnv:TYPO3_SITE_URL
    page.headerData.59.noTrimWrap (
|
<link rel="icon" type="image/png" sizes="192x192" href="|android-icon-192x192.png">
|
    )
[END]


#########################
### Apple touch icons ###
#########################

## apple-icon-57x57.png
[fileExists('apple-icon-57x57.png')]
    page.headerData.60 = TEXT
    page.headerData.60.data = getIndpEnv:TYPO3_SITE_URL
    page.headerData.60.noTrimWrap (
|
<link rel="apple-touch-icon" sizes="57x57" href="|apple-icon-57x57.png">
|
    )
[END]

## apple-icon-60x60.png
[fileExists('apple-icon-60x60.png')]
    page.headerData.61 = TEXT
    page.headerData.61.data = getIndpEnv:TYPO3_SITE_URL
    page.headerData.61.noTrimWrap (
|
<link rel="apple-touch-icon" sizes="60x60" href="|apple-icon-60x60.png">
|
    )
[END]

## apple-icon-72x72.png
[fileExists('apple-icon-72x72.png')]
    page.headerData.62 = TEXT
    page.headerData.62.data = getIndpEnv:TYPO3_SITE_URL
    page.headerData.62.noTrimWrap (
|
<link rel="apple-touch-icon" sizes="72x72" href="|apple-icon-72x72.png">
|
    )
[END]

## apple-icon-76x76.png
[fileExists('apple-icon-76x76.png')]
    page.headerData.63 = TEXT
    page.headerData.63.data = getIndpEnv:TYPO3_SITE_URL
    page.headerData.63.noTrimWrap (
|
<link rel="apple-touch-icon" sizes="76x76" href="|apple-icon-76x76.png">
|
    )
[END]

## apple-icon-114x114.png
[fileExists('apple-icon-114x114.png')]
    page.headerData.64 = TEXT
    page.headerData.64.data = getIndpEnv:TYPO3_SITE_URL
    page.headerData.64.noTrimWrap (
|
<link rel="apple-touch-icon" sizes="114x114" href="|apple-icon-114x114.png">
|
    )
[END]

## apple-icon-120x120.png
[fileExists('apple-icon-120x120.png')]
    page.headerData.65 = TEXT
    page.headerData.65.data = getIndpEnv:TYPO3_SITE_URL
    page.headerData.65.noTrimWrap (
|
<link rel="apple-touch-icon" sizes="120x120" href="|apple-icon-120x120.png">
|
    )
[END]

## apple-icon-57x57.png
[fileExists('apple-icon-144x144.png')]
    page.headerData.66 = TEXT
    page.headerData.66.data = getIndpEnv:TYPO3_SITE_URL
    page.headerData.66.noTrimWrap (
|
<link rel="apple-touch-icon" sizes="144x144" href="|apple-icon-144x144.png">
|
    )
[END]

## apple-icon-152x152.png
[fileExists('apple-icon-152x152.png')]
    page.headerData.67 = TEXT
    page.headerData.67.data = getIndpEnv:TYPO3_SITE_URL
    page.headerData.67.noTrimWrap (
|
<link rel="apple-touch-icon" sizes="152x152" href="|apple-icon-152x152.png">
|
    )
[END]

## apple-icon-180x180.png
[fileExists('apple-icon-180x180.png')]
    page.headerData.68 = TEXT
    page.headerData.68.data = getIndpEnv:TYPO3_SITE_URL
    page.headerData.68.noTrimWrap (
|
<link rel="apple-touch-icon" sizes="180x180" href="|apple-icon-180x180.png">
|
    )
[END]

## apple-icon.png
[fileExists('apple-icon.png')]
    page.headerData.69 = TEXT
    page.headerData.69.data = getIndpEnv:TYPO3_SITE_URL
    page.headerData.69.noTrimWrap (
|
<link rel="apple-touch-icon" sizes="192x192" href="|apple-icon.png">
|
    )
[END]


#####################
### manifest.json ###
#####################

[fileExists('manifest.json')]
    page.headerData.70 = TEXT
    page.headerData.70.data = getIndpEnv:TYPO3_SITE_URL
    page.headerData.70.noTrimWrap (
|
<link rel="manifest" href="|manifest.json">
|
    )
[END]


####################
### MS APP stuff ###
####################

# ToDo: Find working condition for compare string with default constant

## msapplication-TileColor
page.headerData.80 = TEXT
page.headerData.80.value = {$plugin.tx_kitt3nassets.settings.head.msapplication.tile_color}
page.headerData.80.wrap = <meta name="msapplication-TileColor" content="|">

## ms-icon-144x144.png
[fileExists('ms-icon-144x144.png')]
    page.headerData.81 = TEXT
    page.headerData.81.data = getIndpEnv:TYPO3_SITE_URL
    page.headerData.81.dataWrap = <meta name="msapplication-TileImage" content="|ms-icon-144x144.png">
[END]

## theme-color
page.headerData.82 = TEXT
page.headerData.82.value = {$plugin.tx_kitt3nassets.settings.head.theme_color}
page.headerData.82.wrap = <meta name="theme-color" content="|">


#################
## INCLUDE CSS ##
#################

## main.libs.min.css
[fileExists('EXT:kitt3n_custom/Resources/Public/Assets/Main/Css/main.libs.min.css')]
    page.includeCSSLibs.main_libs_css = EXT:kitt3n_custom/Resources/Public/Assets/Main/Css/main.libs.min.css
    page.includeCSSLibs.main_libs_css.media = all
[END]

## main.libs.css
[fileExists('EXT:kitt3n_custom/Resources/Public/Assets/Main/Css/main.libs.min.css') && applicationContext == "Development*"]
    page.includeCSSLibs.main_libs_css = EXT:kitt3n_custom/Resources/Public/Assets/Main/Css/main.libs.css
[END]

## main.min.css
[fileExists('EXT:kitt3n_custom/Resources/Public/Assets/Main/Css/main.min.css')]
    page.includeCSS.main_css = EXT:kitt3n_custom/Resources/Public/Assets/Main/Css/main.min.css
    page.includeCSS.main_css.media = all
[END]

## main.css
[fileExists('EXT:kitt3n_custom/Resources/Public/Assets/Main/Css/main.css') && applicationContext == "Development*"]
    page.includeCSS.main_css = EXT:kitt3n_custom/Resources/Public/Assets/Main/Css/main.css
[END]


################
## INCLUDE JS ##
################

## main.head.libs.min.js
[fileExists('EXT:kitt3n_custom/Resources/Public/Assets/Main/Js/main.head.libs.min.js')]
    page.includeJSLibs.main_head_js = EXT:kitt3n_custom/Resources/Public/Assets/Main/Js/main.head.libs.min.js
[END]

## main.head.libs.js
[fileExists('EXT:kitt3n_custom/Resources/Public/Assets/Main/Js/main.head.libs.js') && applicationContext == "Development*"]
    page.includeJSLibs.main_head_js = EXT:kitt3n_custom/Resources/Public/Assets/Main/Js/main.head.libs.js
[END]

## main.head.min.js
[fileExists('EXT:kitt3n_custom/Resources/Public/Assets/Main/Js/main.head.min.js')]
    page.includeJS.main_head_js = EXT:kitt3n_custom/Resources/Public/Assets/Main/Js/main.head.min.js
[END]

## main.head.js
[fileExists('EXT:kitt3n_custom/Resources/Public/Assets/Main/Js/main.head.js') && applicationContext == "Development*"]
    page.includeJS.main_head_js = EXT:kitt3n_custom/Resources/Public/Assets/Main/Js/main.head.js
[END]

## main.libs.min.js
[fileExists('EXT:kitt3n_custom/Resources/Public/Assets/Main/Js/main.libs.min.js')]
    page.includeJSFooterlibs.main_libs_js = EXT:kitt3n_custom/Resources/Public/Assets/Main/Js/main.libs.min.js
[END]

## main.libs.js
[fileExists('EXT:kitt3n_custom/Resources/Public/Assets/Main/Js/main.libs.js') && applicationContext == "Development*"]
    page.includeJSFooterlibs.main_libs_js = EXT:kitt3n_custom/Resources/Public/Assets/Main/Js/main.libs.js
[END]

## main.min.js
[fileExists('EXT:kitt3n_custom/Resources/Public/Assets/Main/Js/main.min.js')]
    page.includeJSFooter.main_js = EXT:kitt3n_custom/Resources/Public/Assets/Main/Js/main.min.js
[END]

## main.js
[fileExists('EXT:kitt3n_custom/Resources/Public/Assets/Main/Js/main.js') && applicationContext == "Development*"]
    page.includeJSFooter.main_js = EXT:kitt3n_custom/Resources/Public/Assets/Main/Js/main.js
[END]


################
## INCLUDE TS ##
################

## css.headerdata.ts
<INCLUDE_TYPOSCRIPT: source="FILE:EXT:kitt3n_custom/Resources/Public/Assets/Main/TypoScript/css.headerdata.ts" condition="[fileExists('EXT:kitt3n_custom/Resources/Public/Assets/Main/TypoScript/css.headerdata.ts')]">

## js.headerdata.ts
<INCLUDE_TYPOSCRIPT: source="FILE:EXT:kitt3n_custom/Resources/Public/Assets/Main/TypoScript/js.headerdata.ts" condition="[fileExists('EXT:kitt3n_custom/Resources/Public/Assets/Main/TypoScript/js.headerdata.ts')]">

## js.footerdata.ts
<INCLUDE_TYPOSCRIPT: source="FILE:EXT:kitt3n_custom/Resources/Public/Assets/Main/TypoScript/js.footerdata.ts" condition="[fileExists('EXT:kitt3n_custom/Resources/Public/Assets/Main/TypoScript/js.footerdata.ts')]">


#################
## Include AOS ##
#################

[{$plugin.tx_kitt3nassets.settings.includeJSFooterlibs.aos} == '1']
    ## aos.css
    page.includeCSS.aos = /typo3conf/ext/kitt3n_assets/Resources/Public/cdn/cdnjs.cloudflare.com/ajax/libs/aos/2.2.0/aos.css
    page.includeCSS.aos.external = 1
    page.includeCSS.aos.disableCompression = 1
    page.includeCSS.aos.excludeFromConcatenation = 1

    ## aos.js
    page.includeJSFooterlibs.aos = /typo3conf/ext/kitt3n_assets/Resources/Public/cdn/cdnjs.cloudflare.com/ajax/libs/aos/2.2.0/aos.js
    page.includeJSFooterlibs.aos.external = 1
    page.includeJSFooterlibs.aos.disableCompression = 1
    page.includeJSFooterlibs.aos.excludeFromConcatenation = 1

    ## AOS.init();
    page.footerData.1111110 = TEXT
    page.footerData.1111110.value = <script type="text/javascript">
    page.footerData.1111111 = TEXT
    page.footerData.1111111.dataWrap = AOS.init();
    page.footerData.1111112 = TEXT
    page.footerData.1111112.value = </script>
[END]


########################
## Include Hyphenator ##
########################

[{$plugin.tx_kitt3nassets.settings.includeJSFooterlibs.hyphenator} == '1']
    ## Hyphenator.min.js
    page.includeJSFooterlibs.hyphenator = /typo3conf/ext/kitt3n_assets/Resources/Public/cdn/cdnjs.cloudflare.com/ajax/libs/Hyphenator/5.3.0/Hyphenator.min.js
    page.includeJSFooterlibs.hyphenator.external = 1
    page.includeJSFooterlibs.hyphenator.disableCompression = 1
    page.includeJSFooterlibs.hyphenator.excludeFromConcatenation = 1
[END]
