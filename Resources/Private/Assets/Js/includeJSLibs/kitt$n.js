/**
 * TODO:
 * - Add documentation
 * - Add further functions
 */
(function() {
    var kitt$n = function (parameter, context) {
        if (!(this instanceof kitt$n)) {
            return new kitt$n(parameter, context);
        }

        if (parameter instanceof kitt$n) {
            return parameter;
        }

        if (typeof parameter === 'string') {
            parameter = this.select(parameter, context);
        }

        if (parameter && parameter.nodeName) {
            parameter = [parameter];
        }

        this.nodes = this.slice(parameter);
    };

    kitt$n.prototype = {
        get length () {
            return this.nodes.length;
        }
    };

    kitt$n.prototype.nodes = [];

    /**
     * Add html class(es) to the element(s)
     *
     * USAGE:
     * $(selector).addClass('className');
     * $(selector).addClass('className1 className2');
     *
     */
    kitt$n.prototype.addClass = function () {
        return this.eacharg(arguments, function (el, name) {
            el.classList.add(name);
        });
    };

    /** For internal use only */
    kitt$n.prototype.adjacent = function (html, data, callback) {
        if (typeof data === 'number') {
            if (data === 0) {
                data = [];
            } else {
                data = new Array(data).join().split(',').map(Number.call, Number);
            }
        }

        return this.each(function (node, j) {
            var fragment = document.createDocumentFragment();

            kitt$n(data || {}).map(function (el, i) {
                var part = (typeof html === 'function') ? html.call(this, el, i, node, j) : html;

                if (typeof part === 'string') {
                    return this.generate(part);
                }

                return kitt$n(part);
            }).each(function (n) {
                this.isInPage(n)
                    ? fragment.appendChild(kitt$n(n).clone().first())
                    : fragment.appendChild(n);
            });

            callback.call(this, node, fragment);
        });
    };

    kitt$n.prototype.after = function (html, data) {
        return this.adjacent(html, data, function (node, fragment) {
            node.parentNode.insertBefore(fragment, node.nextSibling);
        });
    };

    kitt$n.prototype.append = function (html, data) {
        return this.adjacent(html, data, function (node, fragment) {
            node.appendChild(fragment);
        });
    };

    kitt$n.prototype.appendTo = function (parent) {
        return parent.nodes[0].appendChild(this.nodes[0]);
    };

    /** For internal use only */
    kitt$n.prototype.args = function (args, node, i) {
        if (typeof args === 'function') {
            args = args(node, i);
        }

        if (typeof args !== 'string') {
            args = this.slice(args).map(this.str(node, i));
        }

        return args.toString().split(/[\s,]+/).filter(function (e) {
            return e.length;
        });
    };

    kitt$n.prototype.array = function (callback) {
        callback = callback;
        var self = this;
        return this.nodes.reduce(function (list, node, i) {
            var val;
            if (callback) {
                val = callback.call(self, node, i);
                if (!val) val = false;
                if (typeof val === 'string') val = kitt$n(val);
                if (val instanceof kitt$n) val = val.nodes;
            } else {
                val = node.innerHTML;
            }
            return list.concat(val !== false ? val : []);
        }, []);
    };

    kitt$n.prototype.attr = function (name, value, data) {
        data = data ? 'data-' : '';

        return this.pairs(name, value, function (node, name) {
            return node.getAttribute(data + name);
        }, function (node, name, value) {
            node.setAttribute(data + name, value);
        });
    };

    kitt$n.prototype.before = function (html, data) {
        return this.adjacent(html, data, function (node, fragment) {
            node.parentNode.insertBefore(fragment, node);
        });
    };

    kitt$n.prototype.children = function (selector) {
        return this.map(function (node) {
            return this.slice(node.children);
        }).filter(selector);
    };

    kitt$n.prototype.clone = function () {
        return this.map(function (node, i) {
            var clone = node.cloneNode(true);
            var dest = this.getAll(clone);

            this.getAll(node).each(function (src, i) {
                for (var key in this.mirror) {
                    if (this.mirror[key]) {
                        this.mirror[key](src, dest.nodes[i]);
                    }
                }
            });

            return clone;
        });
    };

    kitt$n.prototype.getAll = function getAll (context) {
        return kitt$n([context].concat(kitt$n('*', context).nodes));
    };

    kitt$n.prototype.mirror = {};

    kitt$n.prototype.mirror.events = function (src, dest) {
        if (!src._e) return;

        for (var type in src._e) {
            src._e[type].forEach(function (event) {
                kitt$n(dest).on(type, event);
            });
        }
    };

    kitt$n.prototype.mirror.select = function (src, dest) {
        if (kitt$n(src).is('select')) {
            dest.value = src.value;
        }
    };

    kitt$n.prototype.mirror.textarea = function (src, dest) {
        if (kitt$n(src).is('textarea')) {
            dest.value = src.value;
        }
    };

    kitt$n.prototype.closest = function (selector) {
        return this.map(function (node) {
            do {
                if (kitt$n(node).is(selector)) {
                    return node;
                }
            } while ((node = node.parentNode) && node !== document);
        });
    };

    function computeStyle(ele, prop, isVariable) {
        if (ele.nodeType !== 1) return;
        var style = win.getComputedStyle(ele, null);
        return prop ? isVariable ? style.getPropertyValue(prop) : style[prop] : style;
    }

    kitt$n.prototype.css = function (prop, value) {
        if (typeof prop === 'string') {
            if (arguments.length < 2) {
                var style = window.getComputedStyle ? getComputedStyle(this.nodes[0], null) : this.nodes[0].currentStyle;
                return this.nodes[0] && style[prop];
            }

            if (!prop) return this;

            return this.each(function (node, i) {
                if (node.nodeType !== 1) return;

                node.style[prop] = value;
            });
        }

        for (var key in prop) {
            this.css(key, prop[key]);
        }

        return this;
    };

    kitt$n.prototype.data = function (name, value) {
        return this.attr(name, value, true);
    };

    kitt$n.prototype.detach = function () {
        return this.each(function (node, i) {
            if (node.parentNode) {
                node.parentNode.removeChild(node);
            }
        });
    };

    kitt$n.prototype.each = function (callback) {
        this.nodes.forEach(callback.bind(this));

        return this;
    };

    /** For internal use only */
    kitt$n.prototype.eacharg = function (args, callback) {
        return this.each(function (node, i) {
            this.args(args, node, i).forEach(function (arg) {
                callback.call(this, node, arg);
            }, this);
        });
    };

    kitt$n.prototype.empty = function () {
        return this.each(function (node) {
            while (node.firstChild) {
                node.removeChild(node.firstChild);
            }
        });
    };

    kitt$n.prototype.fadeIn = function (duration, display) {
        return this.each(function (node, i) {
            var s = node.style, step = 25/(duration || 300);
            s.opacity = s.opacity || 0;
            s.display = display || "block";
            (function fade() { (s.opacity = parseFloat(s.opacity)+step) > 1 ? s.opacity = 1 : setTimeout(fade, 25); })();
        });
    };

    kitt$n.prototype.fadeOut = function (duration) {
        return this.each(function (node, i) {
            var s = node.style, step = 25/(duration || 300);
            s.opacity = s.opacity || 1;
            (function fade() { (s.opacity -= step) < 0 ? s.display = "none" : setTimeout(fade, 25); })();
        });
    };

    kitt$n.prototype.filter = function (selector) {
        var callback = function (node) {
            node.matches = node.matches || node.msMatchesSelector || node.webkitMatchesSelector;

            return node.matches(selector || '*');
        };

        if (typeof selector === 'function') callback = selector;

        if (selector instanceof kitt$n) {
            callback = function (node) {
                return (selector.nodes).indexOf(node) !== -1;
            };
        }

        return kitt$n(this.nodes.filter(callback));
    };

    kitt$n.prototype.find = function (selector) {
        return this.map(function (node) {
            return kitt$n(selector || '*', node);
        });
    };

    kitt$n.prototype.first = function () {
        return this.nodes[0] || false;
    };

    /** For internal use only */
    kitt$n.prototype.generate = function (html) {
        if (/^\s*<tr[> ]/.test(html)) {
            return kitt$n(document.createElement('table')).html(html).children().children().nodes;
        } else if (/^\s*<t(h|d)[> ]/.test(html)) {
            return kitt$n(document.createElement('table')).html(html).children().children().children().nodes;
        } else if (/^\s*</.test(html)) {
            return kitt$n(document.createElement('div')).html(html).children().nodes;
        } else {
            return document.createTextNode(html);
        }
    };

    kitt$n.prototype.handle = function () {
        var args = this.slice(arguments).map(function (arg) {
            if (typeof arg === 'function') {
                return function (e) {
                    e.preventDefault();
                    arg.apply(this, arguments);
                };
            }
            return arg;
        }, this);

        return this.on.apply(this, args);
    };

    kitt$n.prototype.hasClass = function () {
        return this.is('.' + this.args(arguments).join('.'));
    };

    kitt$n.prototype.height = function (value) {
        if (!this.nodes[0]) return value === undefined ? undefined : this;
        var style = window.getComputedStyle ? getComputedStyle(this.nodes[0], null) : this.nodes[0].currentStyle;

        if (value === undefined) {
            return this.nodes[0].getBoundingClientRect()['height'] - (parseInt(style.borderTopWidth) + parseInt(style.paddingTop) + parseInt(style.paddingBottom) + parseInt(style.borderBottomWidth));
        }

        value = parseInt(value, 10);
        return this.each(function (node, i) {
            if (node.nodeType !== 1) return;
            var boxSizing = style.boxSizing;
            node.style['height'] = value + (boxSizing === 'border-box' ? (parseInt(style.borderTopWidth) + parseInt(style.paddingTop) + parseInt(style.paddingBottom) + parseInt(style.borderBottomWidth)) : 0) + 'px';
        });
    };

    kitt$n.prototype.hide = function () {
        return this.each(function (node, i) {
            node.style.display = 'none';
        });
    };

    kitt$n.prototype.html = function (text) {
        if (text === undefined) {
            return this.first().innerHTML || '';
        }

        return this.each(function (node) {
            node.innerHTML = text;
        });
    };

    kitt$n.prototype.innerHeight = function () {
        return this.first().clientHeight;
    };

    kitt$n.prototype.innerWidth = function () {
        return this.first().clientWidth;
    };

    kitt$n.prototype.is = function (selector) {
        return this.filter(selector).length > 0;
    };

    /** For internal use only */
    kitt$n.prototype.isInPage = function isInPage (node) {
        return (node === document.body) ? false : document.body.contains(node);
    };

    kitt$n.prototype.last = function () {
        return this.nodes[this.length - 1] || false;
    };

    kitt$n.prototype.map = function (callback) {
        return callback ? kitt$n(this.array(callback)).unique() : this;
    };

    kitt$n.prototype.next = function () {
        /** TODO: add selector */
        return this.nodes[0] && this.nodes[0].nextElementSibling;
    };

    kitt$n.prototype.not = function (filter) {
        return this.filter(function (node) {
            return !u(node).is(filter || true);
        });
    };

    kitt$n.prototype.off = function (events) {
        return this.eacharg(events, function (node, event) {
            kitt$n(node._e ? node._e[event] : []).each(function (cb) {
                node.removeEventListener(event, cb);
            });
        });
    };

    kitt$n.prototype.offset = function () {
        var el = this.nodes[0];
        if (!el) return;
        var rect = el.getBoundingClientRect();
        return {
            top: rect.top + window.pageYOffset - document.documentElement.clientTop,
            left: rect.left + window.pageXOffset - document.documentElement.clientLeft
        };
    };

    kitt$n.prototype.offsetParent = function () {
        return this.nodes[0] && this.nodes[0].offsetParent;
    };

    kitt$n.prototype.on = function (events, cb, cb2) {
        if (typeof cb === 'string') {
            var sel = cb;
            cb = function (e) {
                var args = arguments;
                kitt$n(e.currentTarget).find(sel).each(function (target) {
                    if (target === e.target || target.contains(e.target)) {
                        try {
                            Object.defineProperty(e, 'currentTarget', {
                                get: function () {
                                    return target;
                                }
                            });
                        } catch (err) {}
                        cb2.apply(target, args);
                    }
                });
            };
        }

        var callback = function (e) {
            return cb.apply(this, [e].concat(e.detail || []));
        };

        return this.eacharg(events, function (node, event) {
            node.addEventListener(event, callback);

            // Store it so we can dereference it with `.off()` later on
            node._e = node._e || {};
            node._e[event] = node._e[event] || [];
            node._e[event].push(callback);
        });
    };

    kitt$n.prototype.outerHeight = function (includeMargins) {
        if (this.nodes[0].nodeType !== 1) return;
        var style = window.getComputedStyle ? getComputedStyle(this.nodes[0], null) : this.nodes[0].currentStyle;
        return this.first().offsetHeight + (includeMargins ? parseInt(style.marginTop) + parseInt(style.marginBottom) : 0);
    };

    kitt$n.prototype.outerWidth = function (includeMargins) {
        if (this.nodes[0].nodeType !== 1) return;
        var style = window.getComputedStyle ? getComputedStyle(this.nodes[0], null) : this.nodes[0].currentStyle;
        return this.first().offsetWidth + (includeMargins ? parseInt(style.marginLeft) + parseInt(style.marginRight) : 0);
    };

    /** For internal use only */
    kitt$n.prototype.pairs = function (name, value, get, set) {
        if (typeof value !== 'undefined') {
            var nm = name;
            name = {};
            name[nm] = value;
        }

        if (typeof name === 'object') {
            return this.each(function (node) {
                for (var key in name) {
                    set(node, key, name[key]);
                }
            });
        }

        return this.length ? get(this.first(), name) : '';
    };

    /** For internal use only */
    kitt$n.prototype.param = function (obj) {
        return Object.keys(obj).map(function (key) {
            return this.uri(key) + '=' + this.uri(obj[key]);
        }.bind(this)).join('&');
    };

    kitt$n.prototype.parent = function (selector) {
        return this.map(function (node) {
            return node.parentNode;
        }).filter(selector);
    };

    kitt$n.prototype.position = function () {
        var el = this.nodes[0];
        if (!el) return;
        return {
            left: el.offsetLeft,
            top: el.offsetTop
        };
    };

    kitt$n.prototype.prepend = function (html, data) {
        return this.adjacent(html, data, function (node, fragment) {
            node.insertBefore(fragment, node.firstChild);
        });
    };

    kitt$n.prototype.prependTo = function (parent) {
        return parent.nodes[0].insertBefore(this.nodes[0], parent.nodes[0].childNodes[0] || null);
    };

    kitt$n.prototype.prev = function () {
        /** TODO: add selector */
        return this.nodes[0] && this.nodes[0].previousElementSibling;
    };

    kitt$n.prototype.prop = function (prop, value) {
        if (!prop) return;

        if (typeof prop === 'string') {
            if (arguments.length < 2) return this.nodes[0] && this.nodes[0][prop];
            return this.each(function (node, i) {
                node[prop] = value;
            });
        }

        for (var key in prop) {
            this.prop(key, prop[key]);
        }

        return this;
    };

    kitt$n.prototype.ready = function (callback) {
        var finalCallback = function finalCallback() {
            return callback();
        };

        if (document.attachEvent ? document.readyState === "complete" : document.readyState !== "loading"){
            setTimeout(finalCallback);
        } else {
            document.addEventListener('DOMContentLoaded', finalCallback);
        }
    };

    kitt$n.prototype.remove = function () {
        return this.each(function (node) {
            if (node.parentNode) {
                node.parentNode.removeChild(node);
            }
        });
    };

    kitt$n.prototype.removeAttr = function () {
        return this.eacharg(arguments, function (el, name) {
            el.removeAttribute(name);
        });
    };

    kitt$n.prototype.removeClass = function () {
        return this.eacharg(arguments, function (el, name) {
            el.classList.remove(name);
        });
    };

    kitt$n.prototype.replace = function (html, data) {
        var nodes = [];
        this.adjacent(html, data, function (node, fragment) {
            nodes = nodes.concat(this.slice(fragment.children));
            node.parentNode.replaceChild(fragment, node);
        });
        return kitt$n(nodes);
    };

    kitt$n.prototype.scroll = function () {
        this.first().scrollIntoView({ behavior: 'smooth' });
        return this;
    };

    /** For internal use only */
    kitt$n.prototype.select = function (parameter, context) {
        parameter = parameter.replace(/^\s*/, '').replace(/\s*$/, '');

        if (/^</.test(parameter)) {
            return kitt$n().generate(parameter);
        }

        return (context || document).querySelectorAll(parameter);
    };

    kitt$n.prototype.serialize = function () {
        var self = this;

        return this.slice(this.first().elements).reduce(function (query, el) {
            if (!el.name || el.disabled || el.type === 'file') return query;

            if (/(checkbox|radio)/.test(el.type) && !el.checked) return query;

            if (el.type === 'select-multiple') {
                kitt$n(el.options).each(function (opt) {
                    if (opt.selected) {
                        query += '&' + self.uri(el.name) + '=' + self.uri(opt.value);
                    }
                });
                return query;
            }

            return query + '&' + self.uri(el.name) + '=' + self.uri(el.value);
        }, '').slice(1);
    };

    kitt$n.prototype.show = function (display) {
        return this.each(function (node, i) {
            node.style.display = display || "block";
        });
    };

    kitt$n.prototype.siblings = function (selector) {
        return this.parent().children(selector).not(this);
    };

    kitt$n.prototype.size = function () {
        return this.first().getBoundingClientRect();
    };

    /** For internal use only */
    kitt$n.prototype.slice = function (pseudo) {
        if (!pseudo ||
            pseudo.length === 0 ||
            typeof pseudo === 'string' ||
            pseudo.toString() === '[object Function]') return [];

        return pseudo.length ? [].slice.call(pseudo.nodes || pseudo) : [pseudo];
    };

    /** For internal use only */
    kitt$n.prototype.str = function (node, i) {
        return function (arg) {
            if (typeof arg === 'function') {
                return arg.call(this, node, i);
            }

            return arg.toString();
        };
    };

    kitt$n.prototype.text = function (text) {
        if (text === undefined) {
            return this.first().textContent || '';
        }

        return this.each(function (node) {
            node.textContent = text;
        });
    };

    kitt$n.prototype.toggleClass = function (classes, addOrRemove) {
        if (!!addOrRemove === addOrRemove) {
            return this[addOrRemove ? 'addClass' : 'removeClass'](classes);
        }

        return this.eacharg(classes, function (el, name) {
            el.classList.toggle(name);
        });
    };

    kitt$n.prototype.trigger = function (events) {
        var data = this.slice(arguments).slice(1);

        return this.eacharg(events, function (node, event) {
            var ev;

            var opts = { bubbles: true, cancelable: true, detail: data };

            try {
                ev = new window.CustomEvent(event, opts);
            } catch (e) {
                ev = document.createEvent('CustomEvent');
                ev.initCustomEvent(event, true, true, data);
            }

            node.dispatchEvent(ev);
        });
    };

    /** For internal use only */
    kitt$n.prototype.unique = function () {
        return kitt$n(this.nodes.reduce(function (clean, node) {
            var istruthy = node !== null && node !== undefined && node !== false;
            return (istruthy && clean.indexOf(node) === -1) ? clean.concat(node) : clean;
        }, []));
    };

    function each(arr, callback) {
        for (var i = 0, l = arr.length; i < l; i++) {
            if (callback.call(arr[i], arr[i], i, arr) === false) break;
        }
    }

    function getValueSelectMultiple(el) {
        var values = [];
        each(el.options, function (option) {
            if (option.selected && !option.disabled && !option.parentNode.disabled) {
                values.push(option.value);
            }
        });
        return values;
    }

    function getValueSelectSingle(el) {
        return el.selectedIndex < 0 ? null : el.options[el.selectedIndex].value;
    }

    var selectOneRe = /select-one/i,
        selectMultipleRe = /select-multiple/i;

    function getValue(el) {
        var type = el.type;
        if (selectOneRe.test(type)) return getValueSelectSingle(el);
        if (selectMultipleRe.test(type)) return getValueSelectMultiple(el);
        return el.value;
    }

    kitt$n.prototype.val = function (value) {
        if (value === undefined) return this.nodes[0] && getValue(this.nodes[0]);
        return this.each(function (node, i) {
            if (selectMultipleRe.test(node.type) && Array.isArray(value)) {
                each(node.options, function (option) {
                    option.selected = value.indexOf(option.value) >= 0;
                });
            } else {
                node.value = value;
            }
        });
    };

    /** For internal use only */
    kitt$n.prototype.uri = function (str) {
        return encodeURIComponent(str).replace(/!/g, '%21').replace(/'/g, '%27').replace(/\(/g, '%28').replace(/\)/g, '%29').replace(/\*/g, '%2A').replace(/%20/g, '+');
    };

    kitt$n.prototype.width = function (value) {
        if (!this.nodes[0]) return value === undefined ? undefined : this;
        var style = window.getComputedStyle ? getComputedStyle(this.nodes[0], null) : this.nodes[0].currentStyle;

        if (value === undefined) {
            return this.nodes[0].getBoundingClientRect()['width'] - (parseInt(style.borderLeftWidth) + parseInt(style.paddingLeft) + parseInt(style.paddingRight) + parseInt(style.borderRightWidth));
        }

        value = parseInt(value, 10);
        return this.each(function (node, i) {
            if (node.nodeType !== 1) return;
            var boxSizing = style.boxSizing;
            node.style['width'] = value + (boxSizing === 'border-box' ? (parseInt(style.borderLeftWidth) + parseInt(style.paddingLeft) + parseInt(style.paddingRight) + parseInt(style.borderRightWidth)) : 0) + 'px';
        });
    };

    kitt$n.prototype.wrap = function (selector) {
        function findDeepestNode (node) {
            while (node.firstElementChild) {
                node = node.firstElementChild;
            }

            return kitt$n(node);
        }

        return this.map(function (node) {
            return kitt$n(selector).each(function (n) {
                findDeepestNode(n)
                    .append(node.cloneNode(true));

                node
                    .parentNode
                    .replaceChild(n, node);
            });
        });
    };

    if (typeof exports !== 'undefined') {
        module.exports = kitt$n;
    } else {
        window.kitt$n = window.$ = kitt$n;
    }
})();